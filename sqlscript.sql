-- REM   Script: SQL Task
-- REM   SQL assignment given by Bala

CREATE TABLE customers( 
    id int PRIMARY KEY, 
    name varchar(255), 
    dob date, 
    gender varchar(10), 
    city varchar(255), 
    check(gender in ('male','female','unknown')) 
);

CREATE TABLE transactions( 
    id int PRIMARY KEY, 
    t_type varchar(10), 
    amount int check(amount > 1 AND amount < 10000), 
    t_datetime timestamp(0) with local time zone default on null current_timestamp(0), 
    t_from int, 
    t_to int, 
    status varchar(50), 
    check(t_type in ('credit','debit')), 
    check(status in ('new','in progress','processed','pending','failed','retry')), 
    FOREIGN KEY(t_from) REFERENCES customers(id), 
    FOREIGN KEY(t_to) REFERENCES customers(id) 
);

DESC customers


DESC transactions


CREATE SEQUENCE cs  
MINVALUE 1  
MAXVALUE 100  
START WITH 1  
INCREMENT BY 1;

INSERT INTO customers (id,name,dob,gender,city) 
VALUES (cs.nextval,'Soumyadeep',DATE '1996-11-13','male','Kolkata');

INSERT INTO customers (id,name,dob,gender,city) 
VALUES (cs.nextval,'Rohit',DATE '1988-10-25','male','Mumbai');

INSERT INTO customers (id,name,dob,gender,city) 
VALUES (cs.nextval,'Vinita',DATE '1970-05-03','female','Bangalore');

INSERT INTO customers (id,name,dob,gender,city) 
VALUES (cs.nextval,'Dilip',DATE '1965-12-04','male','Kolkata');

INSERT INTO customers (id,name,dob,gender,city) 
VALUES (cs.nextval,'Nilima',DATE '2006-07-11','female','Delhi');

INSERT INTO customers (id,name,dob,gender,city) 
VALUES (cs.nextval,'Rita',DATE '1992-04-27','female','Chandigarh');

INSERT INTO customers (id,name,dob,gender,city) 
VALUES (cs.nextval,'Pradip',DATE '1990-11-23','male','Mumbai');

INSERT INTO customers (id,name,dob,gender,city) 
VALUES (cs.nextval,'Nilanjan',DATE '1982-02-16','male','Pune');

CREATE SEQUENCE ts  
MINVALUE 1  
MAXVALUE 100  
START WITH 1  
INCREMENT BY 1;

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'credit',100,2,1,'new');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'debit',2000,1,7,'in progress');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'credit',1900,3,8,'processed');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'debit',750,6,2,'new');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'credit',225,4,7,'processed');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'debit',7500,5,1,'new');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'debit',5000,8,4,'new');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'debit',3900,5,6,'pending');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'credit',500,8,3,'processed');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'credit',800,5,2,'failed');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'credit',9500,2,7,'pending');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'debit',4500,8,7,'in progress');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'debit',6000,1,8,'new');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'credit',1800,3,5,'failed');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'credit',1000,7,8,'retry');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'debit',2000,1,5,'in progress');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'credit',300,2,4,'failed');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'debit',100,8,2,'failed');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'debit',150,3,7,'processed');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'credit',8300,7,3,'processed');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'debit',1700,6,7,'new');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'debit',2700,7,7,'in progress');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'credit',4500,5,8,'pending');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'credit',900,4,6,'failed');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'credit',400,2,1,'processed');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'debit',9000,3,1,'in progress');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'debit',7200,2,2,'in progress');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'debit',3750,3,8,'retry');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'credit',25,7,4,'processed');

INSERT INTO transactions (id,t_type,amount,t_from,t_to,status) 
VALUES (ts.nextval,'credit',150,4,5,'processed');

SELECT customers.*,ROUND((CURRENT_DATE-dob)/365,0) AS age FROM customers 
WHERE ROUND((CURRENT_DATE-dob)/365,0) > 18 AND ROUND((CURRENT_DATE-dob)/365,0) < 32;

SELECT * FROM customers;

SELECT * FROM transactions;

SELECT customers.*,ROUND((CURRENT_DATE-dob)/365,0) AS age FROM customers 
WHERE ROUND((CURRENT_DATE-dob)/365,0) > 18 AND ROUND((CURRENT_DATE-dob)/365,0) < 32;

SELECT * FROM customers  
WHERE city='Mumbai';

SELECT DISTINCT(city) FROM customers;

SELECT * FROM transactions 
WHERE amount < 250;

SELECT * FROM transactions 
WHERE amount > 500 AND t_type = 'credit';

SELECT * FROM transactions 
WHERE status = 'in progress';

SELECT * FROM transactions 
WHERE status = 'pending' AND status = 'retry';

SELECT * FROM transactions 
WHERE status = 'pending' OR status = 'retry';

SELECT * FROM transactions 
WHERE status = 'new' AND t_type = 'debit';

SELECT customers.name AS customer_name,amount,t_datetime,status FROM transactions 
INNER JOIN customers ON transactions.t_from = customers.id 
WHERE city = 'Kolkata' AND t_type = 'debit';

SELECT transactions.*,customers.name AS customer_name,city,gender,dob,ROUND((CURRENT_DATE-dob)/365,0) AS age FROM transactions 
INNER JOIN customers ON transactions.t_from = customers.id 
WHERE ROUND((CURRENT_DATE-dob)/365,0) > 30 AND status = 'failed';

SELECT transactions.*,customers.name AS customer_name,dob,city FROM customers 
INNER JOIN transactions ON transactions.t_from = customers.id 
WHERE gender = 'female' AND t_type = 'debit' AND (status = 'new' OR status = 'retry');